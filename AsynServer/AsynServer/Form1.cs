﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace AsynServer
{ 
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
          
            CheckForIllegalCrossThreadCalls = false;
            TcpListener listener = new TcpListener(IPAddress.Loopback, 11000);
            listener.Start(10);
            listener.BeginAcceptTcpClient(new AsyncCallback(ClientConnect), listener);
        }

        Dictionary<string, TcpClient> lstClients =new Dictionary<string, TcpClient>();
        byte[] b = new byte[1024];
        private void ClientConnect(IAsyncResult ar)
        {
            TcpListener listener =(TcpListener) ar.AsyncState;
            TcpClient client= listener.EndAcceptTcpClient(ar);
            NetworkStream ns = client.GetStream();
            object[] a = new object[2];
            a[0] = ns;
            a[1] = client;
            ns.BeginRead(b, 0, b.Length, new AsyncCallback(ReadMsg), a);
            listener.BeginAcceptTcpClient(new AsyncCallback(ClientConnect), listener);
        }

        private void ReadMsg(IAsyncResult ar)
        {
            object[] a = (object[])ar.AsyncState;
            NetworkStream ns = (NetworkStream) a[0];
            TcpClient client = (TcpClient)a[1];
            int count = ns.EndRead(ar);
            string msg = ASCIIEncoding.ASCII.GetString(b, 0, count);
            if (msg.Contains("@name@"))
            {
                
                string name = msg.Replace("@name@", "");
                lstClients.Add(name, client);
                lstbxClients.Items.Add(name);
            }
            else
            { 
                txtDisplay.Text += msg + Environment.NewLine;
            }
            ns.BeginRead(b, 0, b.Length, new AsyncCallback(ReadMsg), a);
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
  

                try
                {
                    TcpClient client = (TcpClient)lstClients[lstbxClients.SelectedItem.ToString()];
                    NetworkStream ns = client.GetStream();
                    StreamWriter sw = new StreamWriter(ns);
                    string textToSend = "Server Says:" + txtMsg.Text;
                    sw.WriteLine(textToSend);
                    txtDisplay.Text += textToSend + Environment.NewLine;
                    sw.Flush();
                }
                catch (Exception)
                {

                    MessageBox.Show("Please select the CLient.");
                }
            
           
        }

        private void lstbxClients_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var item in lstClients)
            {
                TcpClient client = (TcpClient)item.Value;
                NetworkStream ns = client.GetStream();
                StreamWriter sw = new StreamWriter(ns);
                string textToSend = "Server Says:" + txtMsg.Text;
                sw.WriteLine(textToSend);
                txtDisplay.Text += textToSend + Environment.NewLine;
                sw.Flush();
            }

        }
    }
}
